Wither
======

A fork/continuation of the [Wither](https://dev.bukkit.org/projects/wither)
plugin maintained by CubeKrowd's development team.

Note that as of version **2.2.0**, the configuration file has changed
and must be regenerated! Migration should be very straightforward, but no
automatic migrator is provided.

> Control what players can spawn a Wither, what damage the Wither can do, and
> what worlds the Wither can be spawned in. This plugin will prevent players
> without the proper permission node from creating a Wither by placing Soul
> Sand and Wither Skulls. You are also able to turn off block damage caused by
> Wither and more! This will not stop damage from the Wither when it is
> directed at a Player or Mob.

## Commands

- `/wither` - reload configuration

## Permissions

- `wither.create` - allow creating a wither. **does not bypass** world
  restrictions.
- `wither.reload` - allow reloading the in-memory configuration cache from
  disk.
