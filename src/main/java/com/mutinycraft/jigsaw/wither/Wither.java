package com.mutinycraft.jigsaw.wither;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.java.JavaPlugin;

public class Wither extends JavaPlugin {

    private boolean blockWitherDamage, blockWitherExplosion, blockWitherSpawn;
    private boolean requirePermission;

    private Set<String> worlds;
    private String message;
    private String worldMessage;

    private static final int CONFIG_VERSION = 1;

    @Override
    public void onEnable() {

        saveDefaultConfig();
        reloadConfigCache();

        getServer().getPluginManager().registerEvents(new WitherEventHandler(this), this);
        getCommand("wither").setExecutor(new WitherCommandExecutor(this));

    }

    @Override
    public void onDisable() {
        // empty
    }

    public void reloadConfigCache() {

        reloadConfig();
        int ver = getConfig().getInt("version", 0);

        if (ver != CONFIG_VERSION) {
            getLogger().warning("Config version is " + ver
                    + " but this plugin only supports version "
                    + CONFIG_VERSION + ". Prepare for things to break.");
        }

        blockWitherDamage = getConfig().getBoolean("block-wither-block-damage");
        blockWitherExplosion = getConfig().getBoolean("block-wither-explosions");
        requirePermission = getConfig().getBoolean("require-spawn-permission");

        List<String> worldNames = getConfig().getStringList("block-wither-spawn");
        blockWitherSpawn = worldNames.contains("*");
        worlds = new HashSet<>();
        if (!blockWitherSpawn) {
            worlds.addAll(worldNames);
        }

        message = ChatColor.translateAlternateColorCodes('&',
                getConfig().getString("missing-permission-message"));
        worldMessage = ChatColor.translateAlternateColorCodes('&',
                getConfig().getString("world-blocked-message"));

    }

    public boolean shouldBlockDamage(){
        return blockWitherDamage;
    }

    public boolean shouldBlockExplosion(){
        return blockWitherExplosion;
    }

    public boolean shouldBlockSpawn(Location loc) {
        if (blockWitherSpawn) return true;
        return worlds.contains(loc.getWorld().getName());
    }

    public boolean playerAllowed(Permissible p) {
        if (!requirePermission) return true;
        return p.hasPermission("wither.create");
    }

    public String getMessage(){
        return message;
    }

    public String getWorldMessage(){
        return worldMessage;
    }
}
