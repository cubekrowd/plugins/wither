package com.mutinycraft.jigsaw.wither;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;

public class WitherEventHandler implements Listener {
    private Wither plugin;

    public WitherEventHandler(Wither pl) {
        this.plugin = pl;
    }

    //Wither Eat/Change Block Event
    @EventHandler
    public void onBlockBreak(EntityChangeBlockEvent e) {
        if (e.getEntityType() == EntityType.WITHER && plugin.shouldBlockDamage()) {
            e.setCancelled(true);
        }
    }

    //Wither Eat/Change Hanging Event
    @EventHandler
    public void onPaintingBreak(HangingBreakByEntityEvent e) {
        if (e.getRemover() != null && e.getRemover().getType() == EntityType.WITHER && plugin.shouldBlockDamage()) {
            e.setCancelled(true);
        }
    }

    //Wither Explode Event
    @EventHandler
    public void onExplosionDamage(EntityExplodeEvent e) {
        switch (e.getEntityType()) {
            case WITHER:
            case WITHER_SKULL:
                if (plugin.shouldBlockDamage()) e.blockList().clear();
                if (plugin.shouldBlockExplosion()) e.setCancelled(true);
                break;
            default:
        }
    }

    //Wither Explosion Prime Event
    @EventHandler
    public void onExplosionPrime(ExplosionPrimeEvent e) {
        switch (e.getEntityType()) {
            case WITHER:
            case WITHER_SKULL:
                if (plugin.shouldBlockExplosion()) e.setCancelled(true);
                break;
            default:
        }
    }

    //Wither Spawn Event
    @EventHandler
    public void onWitherSpawn(CreatureSpawnEvent e) {
        if (e.getEntityType() == EntityType.WITHER && plugin.shouldBlockSpawn(e.getLocation())) {
            e.setCancelled(true);
        }
    }

    //Wither Spawn Attempt Event
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!plugin.shouldBlockSpawn(e.getBlock().getLocation())) {
            return;
        }
        // NOTE(traks): filter block placing so players know it's intentional
        // that they can't spawn a wither
        if (!isAllowedChange(e.getBlockPlaced())) {
            if (!plugin.playerAllowed(e.getPlayer())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(plugin.getMessage());
                return;
            }
            if (plugin.shouldBlockSpawn(e.getBlockPlaced().getLocation())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(plugin.getWorldMessage());
                return;
            }
        }
    }

    private boolean isAllowedChange(Block block) {
        return isAllowedChange(block, block.getType());
    }

    private boolean isAllowedChange(Block block, Material mat) {
        switch (mat) {
            case SOUL_SAND:
            case SOUL_SOIL:
                for (var b : new BlockFace[] {BlockFace.UP, BlockFace.NORTH,
                                              BlockFace.EAST, BlockFace.WEST,
                                              BlockFace.SOUTH}) {
                    if (block.getRelative(b).getType() == Material.WITHER_SKELETON_SKULL
                            || block.getRelative(b).getType() == Material.WITHER_SKELETON_WALL_SKULL) {
                        return false;
                    }
                }
                break;
            case WITHER_SKELETON_SKULL:
            case WITHER_SKELETON_WALL_SKULL:
                for (var b : new BlockFace[] {BlockFace.DOWN, BlockFace.NORTH,
                                              BlockFace.EAST, BlockFace.WEST,
                                              BlockFace.SOUTH}) {
                    if (block.getRelative(b).getType() == Material.SOUL_SAND || block.getRelative(b).getType() == Material.SOUL_SOIL) {
                        return false;
                    }
                }
                break;
            default:
                return true;
        }

        return true;
    }

}
