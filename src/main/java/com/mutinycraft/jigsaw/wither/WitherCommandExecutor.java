package com.mutinycraft.jigsaw.wither;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WitherCommandExecutor implements CommandExecutor{

    private Wither plugin;

    public WitherCommandExecutor(Wither pl) {
        plugin = pl;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!label.equalsIgnoreCase("wither")) {
            return true;
        }

        if (sender instanceof Player && !sender.hasPermission("wither.reload")) {
            sender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
            return true;
        }

        plugin.reloadConfigCache();
        sender.sendMessage(ChatColor.GRAY + "Reloaded Wither configuration.");
        return true;

    }

}
